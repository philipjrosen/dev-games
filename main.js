// API ---------------------------------------------------------

var api = {
  url: "https://api.github.com/users",
  name: "github"
}

// Filter Functions for different APIs -------------------------

function githubFilter(data){
  return {
    img: data.avatar_url,
    url: data.html_url,
    text: data.login
  } 
} 

var refreshButton = document.querySelector('.refresh');

var el1 = document.querySelector('.suggestion1');
var el2 = document.querySelector('.suggestion2');
var el3 = document.querySelector('.suggestion3');

var refreshClickStream = Rx.Observable.fromEvent(refreshButton, 'click');
var close1Clicks = Rx.Observable.fromEvent(el1, 'click');
var close2Clicks = Rx.Observable.fromEvent(el2, 'click');
var close3Clicks = Rx.Observable.fromEvent(el3, 'click');

var startupRequestStream = Rx.Observable.just('https://api.github.com/users');

var requestOnRefreshStream = refreshClickStream.map(ev => {
  return api.url;
});

var requestStream = startupRequestStream.merge(requestOnRefreshStream);

var responseStream = requestStream.flatMap(requestUrl => Rx.Observable.fromPromise(jQuery.getJSON(requestUrl))).shareReplay(1);

function getRandomUser(listUsers) {
  return listUsers[Math.floor(Math.random() * listUsers.length)];
}

function createSuggestionStream(responseStream, closeClickStream) {
  return responseStream.map(getRandomUser).startWith(null).merge(refreshClickStream.map(ev => null)).merge(closeClickStream.withLatestFrom(responseStream, (x, R) => getRandomUser(R)));
}

var suggestion1Stream = createSuggestionStream(responseStream, close1Clicks);
var suggestion2Stream = createSuggestionStream(responseStream, close2Clicks);
var suggestion3Stream = createSuggestionStream(responseStream, close3Clicks);



// Rendering ---------------------------------------------------
function renderSuggestion(data, count) {
  
  var template = "<img src='{{img}}'><a href='{{url}}' target='_blank' class='username'>{{text}}</a>";
  
  var $el = $('.suggestion' + count);

  if (data === null) {
    $el.hide();
  } else {
    data = githubFilter(data);
    data.count = count;
    
    $el.html(Mustache.to_html(template, data))
    .show();
    
  }
}

//subscribe to event streams ------------------------------------

suggestion1Stream.subscribe(user => {
  renderSuggestion(user, 1);
});

suggestion2Stream.subscribe(user => {
  renderSuggestion(user, 2);
});

suggestion3Stream.subscribe(user => {
  renderSuggestion(user, 3);
});

